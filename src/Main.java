import service.AquariumService;

public class Main {
    public static void main(String[] args) {
        AquariumService aquariumService = new AquariumService();
        aquariumService.start();
    }
}