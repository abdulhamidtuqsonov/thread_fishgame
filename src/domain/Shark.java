package domain;

import service.AquariumService;
import util.RandomUtil;

public class Shark extends Thread {

    private final AquariumService aquariumService;

    private int age;
    public int x;
    public int y;


    public Shark(AquariumService aquarium) {
        this.aquariumService = aquarium;
    }

    @Override
    public void run() {
        while (age > 0) {
            move();
            aquariumService.checkShark(this);
            try {
                Thread.sleep(1000);
                age--;

            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        System.out.println("RIP SHARK!!");

    }

    private void move() {
        int num = RandomUtil.getRandom(4);
        switch (num) {
            case 1 -> {
                if (y < AquariumService.y) {
                    y++;
                }
            }
            case 2 -> {
                if (x < AquariumService.x) {
                    x++;
                }
            }
            case 3 -> {
                if (y > 0) {
                    y--;
                }
            }
            case 4 -> {
                if (x > 0) {
                    x--;
                }
            }
        }

    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Shark{" +
                ", age=" + age +
                ", x=" + x +
                ", y=" + y +
                '}';
    }
}
