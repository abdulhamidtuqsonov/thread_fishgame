package service;

import domain.Shark;
import util.RandomUtil;

public class SharkService {

    private static final int AGE_MAX = 30; // Maksimal yosh.

    public static Shark getShark(AquariumService aquarium) {
        Shark shark = new Shark(aquarium);
        shark.setAge(RandomUtil.getRandom(AGE_MAX));
        shark.setX(RandomUtil.getRandom(AquariumService.x));
        shark.setY(RandomUtil.getRandom(AquariumService.y));
        return shark;
    }
}
