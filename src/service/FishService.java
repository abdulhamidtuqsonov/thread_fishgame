package service;

import domain.Fish;
import domain.enums.Gender;
import util.RandomUtil;

public class FishService {

    private static final int AGE_MAX = 20; // Maksimal yosh.

    public static Fish getFish(AquariumService aquarium) {
        Fish fish = new Fish(aquarium);
        fish.setAge(RandomUtil.getRandom(AGE_MAX));
        fish.setGender(RandomUtil.getBoolean() ? Gender.MALE : Gender.FEMALE);
        fish.setX(RandomUtil.getRandom(AquariumService.x));
        fish.setY(RandomUtil.getRandom(AquariumService.y));
        return fish;
    }
}
