package service;

import domain.Fish;
import domain.Shark;
import domain.enums.Gender;
import util.RandomUtil;

import java.util.List;
import java.util.ListIterator;
import java.util.Vector;

public class AquariumService {

    public static int x = 5;
    public static int y = 5;
    public static final int totalArea = (x + 1) * (y + 1); // Umumiy maydoni;
    public static final int fishInitialBound = 15; // Baliqning boshlang'ich chegarasi.
    private List<Fish> fishList;


    public void start() {

        fishList = new Vector<>();
        int fishNumber = RandomUtil.getRandom(fishInitialBound);

        for (int i = 0; i < fishNumber; i++) {
            Fish fish = FishService.getFish(this);
            fishList.add(fish);
        }

        ListIterator<Fish> listIterator = fishList.listIterator();
        while (listIterator.hasNext()) {
            listIterator.next().start();
        }

        Shark shark = SharkService.getShark(this);
        shark.start();
        System.out.println(fishList.size());
    }

    public synchronized void checkCollision(Fish fish) {

        if (fishList.size() >= totalArea) {
            System.out.println("___No space"); // Bo'sh joy yo'q.
            return;
        }
        ListIterator<Fish> listIterator = fishList.listIterator();
        while (listIterator.hasNext()) {
            Fish f = listIterator.next();
            if (f.getX() == fish.getX() && f.getY() == fish.getY() && !f.getGender().equals(fish.getGender()) && f.getPuberty() >= 3) {
                Fish bornFish = FishService.getFish(this); // Yangi baliq to'g'ilish.
                System.out.println("___New Fish: " + bornFish);
                bornFish.start();
                listIterator.add(bornFish);
                printStatistics();
            }
        }
    }

    public synchronized void checkShark(Shark shark) {
        ListIterator<Fish> listIterator = fishList.listIterator();
        while (listIterator.hasNext()) {
            Fish f = listIterator.next();
            if (f.getX() == shark.x && f.getY() == shark.y) {
                shark.setAge(shark.getAge() + f.getAge());
                System.out.println("Shark :" + shark);
                listIterator.remove();
                f.setAge(0);
            }
        }
    }


    public synchronized void remove(Fish fish) {
        ListIterator<Fish> listIterator = fishList.listIterator();
        while (listIterator.hasNext()) {
            Fish f = listIterator.next();
            if (f.getName().equals(fish.getName())) {
                System.out.println("___Fish died: " + fish);
                listIterator.remove();
                break;
            }
        }
    }


    public void printStatistics() {
        int male = 0, female = 0;
        for (Fish f : fishList) {
            if (f.getGender().equals(Gender.MALE)) {
                male++;
            } else
                female++;
        }
        System.out.println("___Statistics");
        System.out.println("___Male: " + male);
        System.out.println("___Female: " + female);
        System.out.println("___Total: " + (male + female));
    }
}
