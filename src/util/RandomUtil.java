package util;

import java.util.Random;

public class RandomUtil {

    private static final Random random = new Random();

    public static int getRandom(int num) {
        return random.nextInt(num) + 1; // [0,,,num-1] + 1 --> [1...num]
    }

    public static boolean getBoolean() {
        return random.nextBoolean();
    }
}
